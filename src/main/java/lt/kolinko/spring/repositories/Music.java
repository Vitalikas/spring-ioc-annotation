package lt.kolinko.spring.repositories;

import java.util.List;

public interface Music {
    List<String> getSongs();
}
