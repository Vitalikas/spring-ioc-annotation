package lt.kolinko.spring.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/*
@Configuration indicates that a class declares one or more @Bean methods and may be processed by the Spring container
to generate bean definitions and service requests for those beans at runtime

@ComponentScan enables component (beans) scanning
    basePackages attribute specifies which packages should be scanned for decorated beans

@PropertySource is a convenient mechanism for adding property sources to the environment
    classpath: is a path to "resources" folder
 */

@Configuration
@ComponentScan(basePackages = "lt.kolinko.spring")
@PropertySource("classpath:properties/app.properties")
public class SpringConfig {
}
