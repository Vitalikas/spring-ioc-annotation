package lt.kolinko.spring.services;

import lombok.Getter;
import lt.kolinko.spring.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Random;

// Spring creates bean using annotation @Component
@Component
@Getter
public class MusicPlayer {
    /*
    @Autowired : auto DI by Spring
    @Qualifier : pointer to bean need to be used
    @Value : values injection into variables and method arguments (String, int, double, etc.)
    ${"..."} : value injection via external .properties file
        To resolve ${"..."} in @Value must use @PropertySource
    @PostConstruct : equivalent of init-method
        init-method executes before getting bean from application context
    @PreDestroy : equivalent of destroy-method
        destroy-method executes when application context shuts down
    */

    @Autowired @Qualifier("popMusic") private Music music1;
    @Autowired @Qualifier("technoMusic") private Music music2;
    @Autowired @Qualifier("raveMusic") private Music music3;
    @Autowired @Value("${musicPlayer.name}") private String name;
    @Autowired @Value("${musicPlayer.volume}") private double volume;

    public void playMusic(Genre genre) {
        try {
            switch (genre) {
                case POP:
                    System.out.println("Playing: " + music1.getSongs().get(new Random().nextInt(music1.getSongs().size())));
                    break;
                case TECHNO:
                    System.out.println("Playing: " + music2.getSongs().get(new Random().nextInt(music2.getSongs().size())));
                    break;
                case RAVE:
                    System.out.println("Playing: " + music3.getSongs().get(new Random().nextInt(music3.getSongs().size())));
                    break;
            }
        } catch (NullPointerException e) {
            System.err.println("Nothing to play");
        }
    }

    // init-method equivalent
    @PostConstruct
    public void init() {
        System.out.println("Init method is running here...");
    }

    // destroy-method equivalent
    @PreDestroy
    public void destroy() {
        System.out.println("Destroy method is running here...");
    }
}
