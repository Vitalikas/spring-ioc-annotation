package lt.kolinko.spring;

import lt.kolinko.spring.config.SpringConfig;
import lt.kolinko.spring.repositories.Genre;
import lt.kolinko.spring.services.MusicPlayer;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);

        MusicPlayer musicPlayer = context.getBean("musicPlayer", MusicPlayer.class);

        System.out.print("Enter genre: ");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.next();

        Genre genre = null;
        try {
            genre = Genre.valueOf(input.toUpperCase());
        } catch (IllegalArgumentException e) {
            System.err.printf("There is no %s genre available\n", input);
        }

        assert genre != null;
        musicPlayer.playMusic(genre);

        System.out.println("Name: " + musicPlayer.getName());
        System.out.println("Volume: " + musicPlayer.getVolume() + "%");

        context.close();
    }
}
